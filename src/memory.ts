let image = document.querySelector<HTMLElement>(".image")
let images = document.querySelectorAll<HTMLElement>(".imageCache")
let btnStart = document.querySelector<HTMLElement>(".btnStart")
let Rejouer = document.querySelector<HTMLElement>(".Rejouer")
let body = document.querySelector<HTMLElement>("body")
let tabImage = ["/public/image/cerf.jpg", "/public/image/girafe.jpeg", "/public/image/panda.jpg", "/public/image/panther.jpeg",
    "/public/image/portrait-de-tigre-b414cb5c-b28d-4e13-a956-c8503d730e77.jpg", "/public/image/loup.jpeg", "/public/image/zebre-des-plaines-carre.jpg"
    , "/public/image/dauphin.p.jpg"];
const defaultImage = "/public/image/Memoire3-300x300.jpg"
let tableaux: string[] = [];
let essai: number[] = [];
let reussis: number[] = [];

function chargerTableaux() {
    tableaux = tabImage.concat(tabImage)
    tableaux = tableaux.sort(() => Math.random() - 0.16);
    tableaux = tableaux.sort(() => Math.random() - 0.16);
    tableaux = tableaux.sort(() => Math.random() - 0.16);
    tableaux = tableaux.sort(() => Math.random() - 0.16);
}


function clickImages() {

    for (let animaux of images) {
        animaux.addEventListener('click', (event) => {
            const imageId: number = parseInt(event.target.id)
            if(reussis.includes(imageId)){
                return
            }
            event.target.style.backgroundImage = "url(" + tableaux[imageId] + ")"
            essai.push(imageId)
            verifEssai()
        });


    }

    
}

function verifEssai() {
    if (essai.length < 2) {
        return
    }
console.log(essai);

if(tableaux[essai[0]] == tableaux[essai[1]]){
    reussis.push(essai[0])
    reussis.push(essai[1])
} else {
    const masquerIds = essai
    setTimeout(function(){
      
        
  document.getElementById(String(masquerIds[0])).style.backgroundImage = "url(" + defaultImage + ")"
    document.getElementById(String(masquerIds[1])).style.backgroundImage = "url(" + defaultImage + ")"
     

    },500)
  
}

    essai = []
}

function resetImages() {

    for (let animaux of images) {
        animaux.style.backgroundImage = "url(" + defaultImage + ")"
    
    }
    reussis = []
    essai = []

}
clickImages();

btnStart.addEventListener('click', () => {
    body.classList.remove("backgroundImage")
    Rejouer.style.display = "block"
    btnStart.style.display = "none"
    image.style.display = "block"
    chargerTableaux();
 
    console.log(tableaux)

});
Rejouer.addEventListener('click', () => {
    body.classList.add("backgroundImage")
    Rejouer.style.display = "none"
    btnStart.style.display = "block"
    image.style.display = "none"
    tableaux = []
    resetImages()
console.log(essai);
console.log(reussis);



});










